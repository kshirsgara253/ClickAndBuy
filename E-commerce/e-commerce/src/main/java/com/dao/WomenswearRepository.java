package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.model.Womenswear;

@Repository
public interface WomenswearRepository extends JpaRepository<Womenswear, Integer> {
	
	@Query("from Womenswear w where w.name = :name")
	Womenswear findByName(@Param("name") String name);

}
