package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Kidswear;

@Service
public class KidswearDAO {
	
	@Autowired
	KidswearRepository kidswearRepository;
	

	public List<Kidswear> getAllKidswears(){
		return kidswearRepository.findAll();
	}
	
	public Kidswear getKidswearById(int id){
		return kidswearRepository.findById(id).orElse(null); 
	}
	
	public Kidswear getKidswearByName(String name){
		return kidswearRepository.findByName(name);
	}
	
	public Kidswear registerKidswear(Kidswear kidswear){
		return kidswearRepository.save(kidswear);
	}
	
	public void deleteKidswear(int id){
		kidswearRepository.deleteById(id);
	}


}
