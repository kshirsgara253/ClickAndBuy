package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.SmartWearable;

@Repository
public interface SmartWearableRepository extends JpaRepository<SmartWearable, Integer>{
	
	@Query("from SmartWearable s where s.name = :name")
	SmartWearable findByName(@Param("name") String name);
}
