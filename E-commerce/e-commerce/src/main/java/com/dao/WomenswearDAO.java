package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Womenswear;

@Service
public class WomenswearDAO {
	
	@Autowired
	WomenswearRepository womenswearRepository;
	

	public List<Womenswear> getAllWomenswears(){
		return womenswearRepository.findAll();
	}
	
	public Womenswear getWomenswearById(int id){
		return womenswearRepository.findById(id).orElse(null); 
	}
	
	public Womenswear getWomenswearByName(String name){
		return womenswearRepository.findByName(name);
	}
	
	public Womenswear registerWomenswear(Womenswear womenswear){
		return womenswearRepository.save(womenswear);
	}
	
	public void deleteWomenswear(int womenswearId){
		womenswearRepository.deleteById(womenswearId);
	}


}
