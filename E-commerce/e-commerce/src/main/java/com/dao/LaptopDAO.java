package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Laptop;

@Service
public class LaptopDAO {
	
	@Autowired
	LaptopRepository laptopRepository;
	
	
	public List<Laptop> getAllLaptops(){
		return laptopRepository.findAll();
	}
	
	public Laptop getLaptopById(int id){
		return laptopRepository.findById(id).orElse(null); 
	}
	
	public Laptop getLaptopByName(String name){
		return laptopRepository.findByName(name);
	}
	
	public Laptop registerLaptop(Laptop laptop){
		return laptopRepository.save(laptop);
	}
	
	public void deleteLaptop(int laptopId){
		laptopRepository.deleteById(laptopId);
	}

}
