package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Laptop;

@Repository
public interface LaptopRepository extends JpaRepository<Laptop, Integer>{
	
	@Query("from Laptop l where l.name = :name")
	Laptop findByName(@Param("name") String name);

}
