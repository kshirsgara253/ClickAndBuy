package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	
	@Query("from User u where u.name = :name")
	User findByName(@Param("name") String name);
	
	@Query("from User u where u.emailId = :emailId and u.password = :password")
	User getUser(@Param("emailId") String emailId, @Param("password") String password);
	
	
	@Query("select u from User u where u.emailId = :emailId")
	public User getUserByUsername(@Param("emailId") String emailId);
	
	@Query("select u from User u where u.mobileNo = :mobileNo")
	User getUserByMobileNumber(String mobileNo);
	
}
