package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Accessories;

@Repository
public interface AccessoriesRepository extends JpaRepository<Accessories, Integer> {
	
	@Query("from Accessories a where a.name = :name")
	Accessories findByName(@Param("name") String name);

}
