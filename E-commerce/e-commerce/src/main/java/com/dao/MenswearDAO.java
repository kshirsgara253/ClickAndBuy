package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Menswear;

@Service
public class MenswearDAO {
	
	@Autowired
	MenswearRepository menswearRepository;
	

	public List<Menswear> getAllMenswears(){
		return menswearRepository.findAll();
	}
	
	public Menswear getMenswearById(int id){
		return menswearRepository.findById(id).orElse(null); 
	}
	
	public Menswear getMenswearByName(String name){
		return menswearRepository.findByName(name);
	}
	
	public Menswear registerMenswear(Menswear menswear){
		return menswearRepository.save(menswear);
	}
	
	public void deleteMenswear(int id){
		menswearRepository.deleteById(id);
	}


}
