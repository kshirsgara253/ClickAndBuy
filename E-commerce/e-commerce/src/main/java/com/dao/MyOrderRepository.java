package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.model.MyOrder;

public interface MyOrderRepository extends JpaRepository<MyOrder, Integer>{

	@Query("from MyOrder o where o.orderId = :order_id")
	public MyOrder findByOrderId(@Param("order_id") String order_id);
}
