package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.SmartWearable;

@Service
public class SmartWearableDAO {
	
	@Autowired
	SmartWearableRepository smartWearableRepository;
	
	public List<SmartWearable> getAllSmartWearables(){
		return smartWearableRepository.findAll();
	}
	
	public SmartWearable getSmartWearableById(int id){
		return smartWearableRepository.findById(id).orElse(null); 
	}
	
	public SmartWearable getSmartWearableByName(String name){
		return smartWearableRepository.findByName(name);
	}
	
	public SmartWearable registerSmartWearable(SmartWearable smartWearable){
		return smartWearableRepository.save(smartWearable);
	}
	
	public void deleteSmartWearable(int smartWearableId){
		smartWearableRepository.deleteById(smartWearableId);
	}

}
