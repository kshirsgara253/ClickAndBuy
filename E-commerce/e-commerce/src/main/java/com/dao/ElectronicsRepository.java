package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Electronics;

@Repository
public interface ElectronicsRepository extends JpaRepository<Electronics, Integer> {

	@Query("from Electronics e where e.name = :name")
	Electronics findByName(@Param("name") String name);

}
