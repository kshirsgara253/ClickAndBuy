package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Accessories;

@Service
public class AccessoriesDAO {
	
	@Autowired
	AccessoriesRepository accessoriesRepository;
	
	public List<Accessories> getAllAccessoriess(){
		return accessoriesRepository.findAll();
	}
	
	public Accessories getAccessoriesById(int id){
		return accessoriesRepository.findById(id).orElse(null); 
	}
	
	public Accessories getAccessoriesByName(String name){
		return accessoriesRepository.findByName(name);
	}
	
	public Accessories registerAccessories(Accessories accessories){
		return accessoriesRepository.save(accessories);
	}
	
	public void deleteAccessories(int accessoriesId){
		accessoriesRepository.deleteById(accessoriesId);
	}

}
