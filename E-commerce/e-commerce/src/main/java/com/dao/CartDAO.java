package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Cart;

@Service
public class CartDAO {
	
	@Autowired
	CartRepository cartRepository;
	
	public List<Cart> getCartItems(int userId){
		return cartRepository.findAllByUserId(userId);
	}
	
	public Cart registerCart(Cart cart){
		return cartRepository.save(cart);
	}
	
	public void deleteByUserId(int userId){
		cartRepository.deleteByUserId(userId);
	}

}
