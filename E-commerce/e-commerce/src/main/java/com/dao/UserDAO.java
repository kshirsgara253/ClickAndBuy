package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.User;

@Service
public class UserDAO {
	
	@Autowired
	 UserRepository userRepository;
	
	public List<User> getAllUsers(){
		return userRepository.findAll();
	}
	
	public User getUser(String emailId, String password) {
		return userRepository.getUser(emailId, password);
	}
	
	public User getUserByEmail(String emailId){
		return userRepository.getUserByUsername(emailId);
	}
	
	public User getUserById(int userId){
		return userRepository.findById(userId).orElse(null); 
	}
	
	public User getUserByName(String name){
		return userRepository.findByName(name);
	}
	
	public User registerUser(User user){
		return userRepository.save(user);
	}
	
	public User getUserByMobileNumber(String mobileNo){
		return userRepository.getUserByMobileNumber(mobileNo);
	}
	
	public void deleteUser(int userId){
		userRepository.deleteById(userId);
	}
	
}


