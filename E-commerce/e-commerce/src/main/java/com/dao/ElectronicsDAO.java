package com.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Electronics;

@Service
public class ElectronicsDAO {
	
	@Autowired
	ElectronicsRepository electronicsRepository;
	
	public List<Electronics> getAllElectronics(){
		return electronicsRepository.findAll();
	}
	
	public Electronics getElecrtonicsById(int electronicsId){
		return electronicsRepository.findById(electronicsId).orElse(null);
	}
	
	public Electronics getElectronicsByName(String name){
		return electronicsRepository.findByName(name);
	}
	public Electronics registerElectronics(Electronics electronics){
		return electronicsRepository.save(electronics);
	}
	
	public void deleteElectronics(int electronicsId){
		 electronicsRepository.deleteById(electronicsId);
	}
	
	

}
