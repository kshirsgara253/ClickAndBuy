package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Mobile;


@Service
public class MobileDAO {
	
	@Autowired
	MobileRepository mobileRepository;
	
	public List<Mobile> getAllMobiles(){
		return mobileRepository.findAll();
	}
	
	public Mobile getMobileById(int id){
		return mobileRepository.findById(id).orElse(null); 
	}
	
	public Mobile getMobileByName(String name){
		return mobileRepository.findByName(name);
	}
	
	public Mobile registerMobile(Mobile mobile){
		return mobileRepository.save(mobile);
	}
	
	public void deleteMobile(int mobileId){
		mobileRepository.deleteById(mobileId);
	}
	
	

}
