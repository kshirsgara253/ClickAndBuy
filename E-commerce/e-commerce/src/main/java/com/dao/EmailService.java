package com.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {
    @Autowired
    private JavaMailSender mailSender;

    public String sendOtpEmail(String toEmail, String otp) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("wasimahmed15042000@gmail.com");
        message.setTo(toEmail);
        message.setSubject("Email Verification");
        message.setText("Your OTP for ClickAndBuy: " + otp);

        mailSender.send(message);
        return otp;
    }
}

