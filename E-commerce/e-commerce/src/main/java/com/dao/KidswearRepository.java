package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Kidswear;
import com.model.Menswear;

@Repository
public interface KidswearRepository extends JpaRepository<Kidswear, Integer> {
	
	@Query("from Kidswear k where k.name = :name")
	Kidswear findByName(@Param("name") String name);
}
