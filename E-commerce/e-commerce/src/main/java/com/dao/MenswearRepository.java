package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Menswear;
import com.model.Womenswear;

@Repository
public interface MenswearRepository extends JpaRepository<Menswear, Integer>{
	
	@Query("from Menswear m where m.name = :name")
	Menswear findByName(@Param("name") String name);
}
