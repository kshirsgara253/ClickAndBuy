package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Mobile;

@Repository
public interface MobileRepository extends JpaRepository<Mobile, Integer>{

	@Query("from Mobile m where m.name = :name")
	Mobile findByName(@Param("name") String name);
}
