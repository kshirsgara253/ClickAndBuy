package com.model;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Electronics {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO )
	private int electronicsId;
	private String name;
	

	public Electronics() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Electronics(int electronicsId, String name) {
		super();
		this.electronicsId = electronicsId;
		this.name = name;
	}

	public int getId() {
		return electronicsId;
	}

	public void setId(int electronicsId) {
		this.electronicsId = electronicsId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Electronics [id=" + electronicsId + ", name=" + name + "]";
	}
	

}
