package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Womenswear {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String name;
	private double price;
	private String size;
	private String description;
	private String imagePath;
	public Womenswear() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Womenswear(String name) {
		super();
		this.name = name;
	}

	public Womenswear(int id, String name, double price, String size, String description, String imagePath) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.size = size;
		this.description = description;
		this.imagePath = imagePath;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	@Override
	public String toString() {
		return "Womenswear [id=" + id + ", name=" + name + ", price=" + price + ", size=" + size + ", description="
				+ description + ", imagePath=" + imagePath + "]";
	}

	
}