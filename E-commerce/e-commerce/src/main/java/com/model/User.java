package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int userId;
	private String name;
	private String gender;
	@Column(unique = true)
	private String emailId;
	@Column(unique = true)
	private String mobileNo;
	private String password;
	private String address;
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public User(int userId, String name, String gender, String emailId, String mobileNo, String password,
			String address) {
		super();
		this.userId = userId;
		this.name = name;
		this.gender = gender;
		this.emailId = emailId;
		this.mobileNo = mobileNo;
		this.password = password;
		this.address = address;
	}
	public User(String name) {
		super();
		this.name = name;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "User [userId=" + userId + ", name=" + name + ", gender=" + gender + ", emailId=" + emailId
				+ ", mobileNo=" + mobileNo + ", password=" + password + ", address=" + address + "]";
	}
}
	