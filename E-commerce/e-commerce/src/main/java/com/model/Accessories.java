package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Accessories {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String name;
	private String imagePath;
	private double price;
	private String description;
	private String additionalInfo;
	
	
	@ManyToOne
    @JoinColumn(name = "electronicsId")
    private Electronics electronics;

	public Accessories() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public Accessories(String name) {
		super();
		this.name = name;
	}


	public Accessories(int id, String name, String imagePath, double price, String description, String additionalInfo) {
		super();
		this.id = id;
		this.name = name;
		this.imagePath = imagePath;
		this.price = price;
		this.description = description;
		this.additionalInfo = additionalInfo;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getImagePath() {
		return imagePath;
	}


	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(double price) {
		this.price = price;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getAdditionalInfo() {
		return additionalInfo;
	}


	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}


	public Electronics getElectronics() {
		return electronics;
	}


	public void setElectronics(Electronics electronics) {
		this.electronics = electronics;
	}


	@Override
	public String toString() {
		return "Accessories [id=" + id + ", name=" + name + ", imagePath=" + imagePath + ", price=" + price
				+ ", description=" + description + ", additionalInfo=" + additionalInfo + ", electronics=" + electronics
				+ "]";
	}
}
