package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Kidswear {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String name;
	private String size;
	private double price;
	private String description;
	private String imagePath;
	public Kidswear() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Kidswear(String name) {
		super();
		this.name = name;
	}

	public Kidswear(int id, String name, String size, double price, String description, String imagePath) {
		super();
		this.id = id;
		this.name = name;
		this.size = size;
		this.price = price;
		this.description = description;
		this.imagePath= imagePath;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	@Override
	public String toString() {
		return "Kidswear [id=" + id + ", name=" + name + ", size=" + size + ", price=" + price + ", description="
				+ description + ", imagePath=" + imagePath + "]";
	}

	
	
	

}