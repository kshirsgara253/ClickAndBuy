package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Laptop {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO )
	private int id;
	private String name;
	private String imagePath;
	private double price;
	private String description;
	private String batteryInfo;
	private String operatingInfo;
	
	
	
	@ManyToOne
    @JoinColumn(name = "electronicsId")
    private Electronics electronics;

	public Laptop() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public Laptop(String name) {
		super();
		this.name = name;
	}



	public Laptop(int id, String name, String imagePath, double price, String description, String batteryInfo,
			String operatingInfo) {
		super();
		this.id = id;
		this.name = name;
		this.imagePath = imagePath;
		this.price = price;
		this.description = description;
		this.batteryInfo = batteryInfo;
		this.operatingInfo = operatingInfo;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getImagePath() {
		return imagePath;
	}



	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}



	public double getPrice() {
		return price;
	}



	public void setPrice(double price) {
		this.price = price;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getBatteryInfo() {
		return batteryInfo;
	}



	public void setBatteryInfo(String batteryInfo) {
		this.batteryInfo = batteryInfo;
	}



	public String getOperatingInfo() {
		return operatingInfo;
	}



	public void setOperatingInfo(String operatingInfo) {
		this.operatingInfo = operatingInfo;
	}



	public Electronics getElectronics() {
		return electronics;
	}



	public void setElectronics(Electronics electronics) {
		this.electronics = electronics;
	}



	@Override
	public String toString() {
		return "Laptop [id=" + id + ", name=" + name + ", imagePath=" + imagePath + ", price=" + price
				+ ", description=" + description + ", batteryInfo=" + batteryInfo + ", operatingInfo=" + operatingInfo
				+ ", electronics=" + electronics + "]";
	}

	

}
