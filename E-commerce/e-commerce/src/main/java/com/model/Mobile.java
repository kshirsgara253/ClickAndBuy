package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Mobile {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int mobileId;

    private String name;
    private double price;
    private String imagePath;
    private String description;
    
    private String batteryInfo;
    private String  networkType;
    private String warrentyInfo;
    
    

    @ManyToOne
    @JoinColumn(name = "electronicsId")
    private Electronics electronics;
    
    

	public Mobile() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Mobile(String name) {
		super();
		this.name = name;
	}


	public Mobile(int mobileId, String name, double price, String imagePath, String description, String batteryInfo,
			String networkType, String warrentyInfo) {
		super();
		this.mobileId = mobileId;
		this.name = name;
		this.price = price;
		this.imagePath = imagePath;
		this.description = description;
		this.batteryInfo = batteryInfo;
		this.networkType = networkType;
		this.warrentyInfo = warrentyInfo;
	}
	

	public int getMobileId() {
		return mobileId;
	}

	public void setMobileId(int mobileId) {
		this.mobileId = mobileId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	public String getBatteryInfo() {
		return batteryInfo;
	}
	public void setBatteryInfo(String batteryInfo) {
		this.batteryInfo = batteryInfo;
	}



	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
	public String getWarrentyInfo() {
		return warrentyInfo;
	}
	public void setWarrentyInfo(String warrentyInfo) {
		this.warrentyInfo = warrentyInfo;
	}

	public Electronics getElectronics() {
		return electronics;
	}

	public void setElectronics(Electronics electronics) {
		this.electronics = electronics;
	}

	@Override
	public String toString() {
		return "Mobile [mobileId=" + mobileId + ", name=" + name + ", price=" + price + ", imagePath=" + imagePath
				+ ", description=" + description + ", batteryInfo=" + batteryInfo + ", networkType=" + networkType
				+ ", warrentyInfo=" + warrentyInfo + ", electronics=" + electronics + "]";
	}


}

