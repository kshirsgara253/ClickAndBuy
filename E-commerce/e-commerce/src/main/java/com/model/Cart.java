package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Cart {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	String name;
	
	double price;
	String description;
	
	@ManyToOne
    @JoinColumn(name = "userId")
    private User user;

	public Cart() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Cart(int id, String name,  double price, String description) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.description = description;
		
	}

	public Cart(String name) {
		super();
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Cart [id=" + id + ", name=" + name + ", price=" + price + ", description="
				+ description + ", user=" + user + "]";
	}

	
	

}
