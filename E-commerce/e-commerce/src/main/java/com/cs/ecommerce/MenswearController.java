package com.cs.ecommerce;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.MenswearDAO;
import com.model.Menswear;

@RestController
public class MenswearController {
	
	@Autowired
	MenswearDAO menswearDAO;
	
	@GetMapping("/getAllMenswear")
	public List<Menswear> getAllMenswear(){
		return menswearDAO.getAllMenswears();
	}

	
	@GetMapping("/getMenswearById/{id}")
	public Menswear geMenswearById(@PathVariable("id") int id){
		Menswear wear = menswearDAO.getMenswearById(id);
		
		if(wear != null){
			return wear;
		}
		return new Menswear("Not found");
		
	}
	
	@GetMapping("/getMenswearByName/{name}")
	public Menswear getMenswearByName(@PathVariable("name") String name){
		Menswear wear = menswearDAO.getMenswearByName(name);
		
		if(wear != null){
			return wear;
			
		}
		return new Menswear("Not found");
	}
	
	@PostMapping("/registerMenswear")
	public String registerMenswear(@RequestBody Menswear menswear){
		Menswear wear = menswearDAO.registerMenswear(menswear);
		if(wear != null){
			return "Menswear registered into the database";
			
		}
		return "Menswear registration failed!!!";
	}
	
	@PutMapping("/updateMenswear")
	public String updateMenswear(@RequestBody Menswear menswear){
		Menswear wear = menswearDAO.registerMenswear(menswear);
		if(wear != null){
			return "Menswear record updated in the database";
		}
		return "Menswear updation failed!!!";
	}
	
	@DeleteMapping("/deleteMenswear/{id}")
	public String deleteMenswear(@PathVariable("laptopId") int id){
		menswearDAO.deleteMenswear(id);
		return "Menswear(" + id + ") record deleted Successfully!";
	}
	
}
