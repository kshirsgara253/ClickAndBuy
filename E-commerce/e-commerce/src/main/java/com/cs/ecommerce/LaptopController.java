package com.cs.ecommerce;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.LaptopDAO;
import com.model.Laptop;

@RestController
public class LaptopController {
	
	@Autowired
	LaptopDAO laptopDAO;
	
	@GetMapping("/getAllLaptop")
		public List<Laptop> getAllLaptop(){
			return laptopDAO.getAllLaptops();
		}
	
	
	@GetMapping("/getLaptopById/{laptopId}")
	public Laptop geLaptopById(@PathVariable("laptopId") int laptopId){
		Laptop lapt = laptopDAO.getLaptopById(laptopId);
		
		if(lapt != null){
			return lapt;
		}
		return new Laptop("Not found");
		
	}
	
	@GetMapping("/getLaptopByName/{laptopName}")
	public Laptop getLaptopByName(@PathVariable("laptopName") String laptopName){
		Laptop lapt = laptopDAO.getLaptopByName(laptopName);
		
		if(lapt != null){
			return lapt;
			
		}
		return new Laptop("Not found");
	}
	
	@PostMapping("/registerLaptop")
	public String registerLaptop(@RequestBody Laptop laptop){
		Laptop lapt = laptopDAO.registerLaptop(laptop);
		if(lapt != null){
			return "Laptop registered into the database";
			
		}
		return "Laptop registration failed!!!";
	}
	
	@PutMapping("/updateLaptop")
	public String updateLaptop(@RequestBody Laptop laptop){
		Laptop lapt = laptopDAO.registerLaptop(laptop);
		if(lapt != null){
			return "Laptop record updated in the database";
		}
		return "Laptop updation failed!!!";
	}
	
	@DeleteMapping("/deleteLaptop/{laptopId}")
	public String deleteLaptop(@PathVariable("laptopId") int laptopId){
		laptopDAO.deleteLaptop(laptopId);
		return "Laptop(" + laptopId + ") record deleted Successfully!";
	}
	
}
