package com.cs.ecommerce;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ElectronicsDAO;
import com.model.Electronics;

@RestController
public class ElectronicsController {
	
	@Autowired
	ElectronicsDAO electronicsDAO;
	
	@GetMapping("/getAllElectronics")
		public List<Electronics> getAllElectronics(){
			return electronicsDAO.getAllElectronics();
		}
	
	
	@GetMapping("/getElectronicsById/{electronicsId}")
	public Electronics geElectronicsById(@PathVariable("electronicsId") int electronicsId){
		Electronics elect = electronicsDAO.getElecrtonicsById(electronicsId);
		
		if(elect != null){
			return elect;
		}
		return new Electronics(0, "Not found");
		
	}
	
	@GetMapping("/getElectronicsByName/{electronicsName}")
	public Electronics getElectronicsByName(@PathVariable("electronicsName") String electronicsName){
		Electronics elect = electronicsDAO.getElectronicsByName(electronicsName);
		
		if(elect != null){
			return elect;
			
		}
		return new Electronics(0, "Not found");
	}
	
	@PostMapping("/registerElectronics")
	public String registerElectronics(@RequestBody Electronics electronics){
		Electronics elect = electronicsDAO.registerElectronics(electronics);
		if(elect != null){
			return "Electronics registered into the database";
			
		}
		return "Electronics registration failed!!!";
	}
	
	@PutMapping("/updateElectronics")
	public String updateElectronics(@RequestBody Electronics electronics){
		Electronics elect = electronicsDAO.registerElectronics(electronics);
		if(elect != null){
			return "Electronics record updated in the database";
		}
		return "Electronics updation failed!!!";
	}
	
	@DeleteMapping("/deleteElectronics/{electronicsId}")
	public String deleteElectronics(@PathVariable("electronicsId") int electronicsId){
		electronicsDAO.deleteElectronics(electronicsId);
		return "Electronics(" + electronicsId + ") record deleted Successfully!";
	}
}
	
