package com.cs.ecommerce;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.MobileDAO;
import com.model.Mobile;

@RestController
public class MobileController {
	
	@Autowired
	MobileDAO mobileDAO;
	
	@GetMapping("/getAllMobile")
		public List<Mobile> getAllMobile(){
			return mobileDAO.getAllMobiles();
		}
	
	
	@GetMapping("/getMobileById/{mobileId}")
	public Mobile geMobileById(@PathVariable("mobileId") int mobileId){
		Mobile lapt = mobileDAO.getMobileById(mobileId);
		
		if(lapt != null){
			return lapt;
		}
		return new Mobile("Not found");
		
	}
	
	@GetMapping("/getMobileByName/{mobileName}")
	public Mobile getMobileByName(@PathVariable("mobileName") String mobileName){
		Mobile mob = mobileDAO.getMobileByName(mobileName);
		
		if(mob != null){
			return mob;
			
		}
		return new Mobile("Not found");
	}
	
	@PostMapping("/registerMobile")
	public String registerMobile(@RequestBody Mobile mobile){
		Mobile mob = mobileDAO.registerMobile(mobile);
		if(mob != null){
			return "Mobile registered into the database";
			
		}
		return "Mobile registration failed!!!";
	}
	
	@PutMapping("/updateMobile")
	public String updateMobile(@RequestBody Mobile mobile){
		Mobile mob = mobileDAO.registerMobile(mobile);
		if(mob != null){
			return "Mobile record updated in the database";
		}
		return "Mobile updation failed!!!";
	}
	
	@DeleteMapping("/deleteMobile/{mobileId}")
	public String deleteMobile(@PathVariable("mobileId") int mobileId){
		mobileDAO.deleteMobile(mobileId);
		return "Mobile(" + mobileId + ") record deleted Successfully!";
	}
	
}

