package com.cs.ecommerce;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.SmartWearableDAO;

import com.model.SmartWearable;

@RestController
public class SmartWearableController {
	
	@Autowired
	SmartWearableDAO smartWearableDAO;
	
	@GetMapping("/getAllSmartWearables")
		public List<SmartWearable> getAllSmartWearable(){
			return smartWearableDAO.getAllSmartWearables();
		}
	
	
	@GetMapping("/getSmartWearableById/{smartWearableId}")
	public SmartWearable getSmartWearableById(@PathVariable("smartWearableId") int smartWearableId){
		SmartWearable smtwlb = smartWearableDAO.getSmartWearableById(smartWearableId);
		
		if(smtwlb != null){
			return smtwlb;
		}
		return new SmartWearable("Not found");
		
	}
	
	@GetMapping("/getSmartWearableByName/{smartWearableName}")
	public SmartWearable getSmartWearableByName(@PathVariable("smartWearableName") String smartWearableName){
		SmartWearable smtwlb = smartWearableDAO.getSmartWearableByName(smartWearableName);
		
		if(smtwlb != null)
			return smtwlb;
			
		return new SmartWearable("Not found");
	}
	
	@PostMapping("/registerSmartWearable")
	public String registerSmartWearable(@RequestBody SmartWearable smartWearable){
		SmartWearable smtwlb = smartWearableDAO.registerSmartWearable(smartWearable);
		if(smtwlb != null){
			return "SmartWearable registered into the database";
			
		}
		return "SmartWearable registration failed!!!";
	}
	
	@PutMapping("/updateSmartWearable")
	public String updateSmartWearable(@RequestBody SmartWearable smartWearable){
		SmartWearable smtwlb = smartWearableDAO.registerSmartWearable(smartWearable);
		if(smtwlb != null){
			return "SmartWearable record updated in the database";
		}
		return "SmartWearable updation failed!!!";
	}
	
	@DeleteMapping("/deleteSmartWearable/{smartWearableId}")
	public String deleteSmartWearable(@PathVariable("smartWearableId") int smartWearableId){
		smartWearableDAO.deleteSmartWearable(smartWearableId);
		return "SmartWearable(" + smartWearableId + ") record deleted Successfully!";
	}

}
