package com.cs.ecommerce;

import java.security.Principal;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


import org.springframework.web.bind.annotation.RestController;

import com.dao.MyOrderRepository;
import com.dao.UserDAO;
import com.dao.UserRepository;
import com.model.MyOrder;
import com.model.User;
import com.razorpay.Order;

import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;

@RestController
public class PaymentController {
	@Autowired
	 UserRepository userRepository;
	
	@Autowired
	UserDAO userDAO;
	
	@Autowired
	MyOrderRepository myOrderRepository;

  @PostMapping("/create_order/{userId}")
  public String createOrder(@RequestBody Map<String, Object> data, @PathVariable("userId") int userId) throws Exception{
    int amount = Integer.parseInt(data.get("amount").toString());
    Order order = null;
    try{
      RazorpayClient client = new RazorpayClient("rzp_test_GbZAJqpexP0OCj", "8NeYD88QsvTdjdEtTx8xaJAC");

      JSONObject orderRequest = new JSONObject();
      orderRequest.put("amount", amount * 100); // amount in the smallest currency unit
      orderRequest.put("currency", "INR");
      orderRequest.put("receipt", "txn_234523");

      order = client.orders.create(orderRequest);
      // save order in database
      User user = this.userDAO.getUserById(userId);
      
      
      MyOrder myOrder = new MyOrder();
      myOrder.setAmount(order.get("amount")+"");
      myOrder.setOrderId(order.get("id"));
      myOrder.setPaymentId(null);
      myOrder.setStatus("created");
      myOrder.setUser(user);
      myOrder.setReceipt(order.get("receipt"));
      
      this.myOrderRepository.save(myOrder);
    } catch(RazorpayException e) {
      System.out.println(e.getMessage());
    }
    return order.toString();
  }
  
  @PostMapping("/update_order")
  public ResponseEntity<?> updateOrder(@RequestBody Map<String, Object> data){
	  MyOrder myOrder = this.myOrderRepository.findByOrderId(data.get("order_id").toString());
	  myOrder.setPaymentId(data.get("payment_id").toString());
	  myOrder.setStatus("paid");
	  System.out.println(data);
	  this.myOrderRepository.save(myOrder);
	  return ResponseEntity.ok("updated");
  }
}



	

	


	
	
	


