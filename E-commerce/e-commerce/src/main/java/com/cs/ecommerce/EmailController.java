package com.cs.ecommerce;




import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



import com.mailjet.client.errors.MailjetException;
import com.mailjet.client.errors.MailjetSocketTimeoutException;
import com.mailjet.client.MailjetClient;
import com.mailjet.client.MailjetRequest;
import com.mailjet.client.MailjetResponse;
import com.mailjet.client.ClientOptions;
import com.mailjet.client.resource.Emailv31;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@RestController
public class EmailController {
//    @Autowired
//    private EmailService emailService;

//    @GetMapping("/send-otp/{toEmail}")
//    public String sendOtp(@PathVariable("toEmail") String toEmail) {
//        int otp = 100000 + new Random().nextInt(900000);
//        return emailService.sendOtpEmail(toEmail, Integer.toString(otp));
////        return new ResponseEntity<>("OTP sent successfully!", HttpStatus.OK);
//    }
    
    @GetMapping(value = "/sendMail")
	 public ResponseEntity<String> sendMail(@RequestParam("toEmail") String toEmail)throws MailjetException, JSONException, MailjetSocketTimeoutException
	 {
		 System.out.println(toEmail);
		 MailjetClient client;
		    MailjetRequest request;
		    MailjetResponse response;
		    client = new MailjetClient("f43aa24563b5fe012e28231267b23a25", "b13fe809f112034df5574b732e0c1640", new ClientOptions("v3.1"));
		    final String OTP_TO_VERIFY = SmsController.generateOTP();
		    request = new MailjetRequest(Emailv31.resource)
		    .property(Emailv31.MESSAGES, new JSONArray()
		    .put(new JSONObject()
		    .put(Emailv31.Message.FROM, new JSONObject()
		    .put("Email", "kshirsagara253@gmail.com")
		    .put("Name", "Aditya"))
		    .put(Emailv31.Message.TO, new JSONArray()
		    .put(new JSONObject()
		    .put("Email", toEmail)
		    .put("Name", "Aditya")))
		    .put(Emailv31.Message.SUBJECT, "Click And Buy OTP")
		    .put(Emailv31.Message.TEXTPART, "Your OTP is " + OTP_TO_VERIFY)
		    .put(Emailv31.Message.HTMLPART, "Your OTP is " + OTP_TO_VERIFY)
		    .put(Emailv31.Message.CUSTOMID, "AppGettingStartedTest")));
		    response = client.post(request);
		    System.out.println(response.getStatus());
		    System.out.println(response.getData());
		    
		    return new ResponseEntity<String>(OTP_TO_VERIFY, HttpStatus.OK);
	 }
}




