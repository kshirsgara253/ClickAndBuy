package com.cs.ecommerce;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.WomenswearDAO;
import com.model.Womenswear;

@RestController
public class WomanwearController {
	
	@Autowired
	WomenswearDAO womenswearDAO;
	
	@GetMapping("/getAllWomenswear")
	public List<Womenswear> getAllWomenswear(){
		return womenswearDAO.getAllWomenswears();
	}

	
	@GetMapping("/getWomenswearById/{id}")
	public Womenswear geWomenswearById(@PathVariable("id") int id){
		Womenswear wear = womenswearDAO.getWomenswearById(id);
		
		if(wear != null){
			return wear;
		}
		return new Womenswear("Not found");
		
	}
	
	@GetMapping("/getWomenswearByName/{name}")
	public Womenswear getWomenswearByName(@PathVariable("name") String name){
		Womenswear wear = womenswearDAO.getWomenswearByName(name);
		
		if(wear != null){
			return wear;
			
		}
		return new Womenswear("Not found");
	}
	
	@PostMapping("/registerWomenswear")
	public String registerWomenswear(@RequestBody Womenswear womenswear){
		Womenswear wear = womenswearDAO.registerWomenswear(womenswear);
		if(wear != null){
			return "Womenswear registered into the database";
			
		}
		return "Womenswear registration failed!!!";
	}
	
	@PutMapping("/updateWomenswear")
	public String updateWomenswear(@RequestBody Womenswear womenswear){
		Womenswear wear = womenswearDAO.registerWomenswear(womenswear);
		if(wear != null){
			return "Womenswear record updated in the database";
		}
		return "Womenswear updation failed!!!";
	}
	
	@DeleteMapping("/deleteWomenswear/{id}")
	public String deleteWomenswear(@PathVariable("id") int id){
		womenswearDAO.deleteWomenswear(id);
		return "Womenswear(" + id + ") record deleted Successfully!";
	}
	
	}

