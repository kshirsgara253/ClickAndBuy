package com.cs.ecommerce;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.KidswearDAO;
import com.model.Kidswear;

@RestController
public class KidswearController {
	
	@Autowired
	KidswearDAO kidswearDAO;
	
	@GetMapping("/getAllKidswear")
	public List<Kidswear> getAllKidswear(){
		return kidswearDAO.getAllKidswears();
	}


	@GetMapping("/getKidswearById/{id}")
	public Kidswear geKidswearById(@PathVariable("id") int id){
		Kidswear wear = kidswearDAO.getKidswearById(id);
		
		if(wear != null){
			return wear;
		}
		return new Kidswear("Not found");
		
	}
	
	@GetMapping("/getKidswearByName/{name}")
	public Kidswear getKidswearByName(@PathVariable("name") String name){
		Kidswear wear = kidswearDAO.getKidswearByName(name);
		
		if(wear != null){
			return wear;
			
		}
		return new Kidswear("Not found");
	}
	
	@PostMapping("/registerKidswear")
	public String registerKidswear(@RequestBody Kidswear kidswear){
		Kidswear wear = kidswearDAO.registerKidswear(kidswear);
		if(wear != null){
			return "Kidswear registered into the database";
			
		}
		return "Kidswear registration failed!!!";
	}
	
	@PutMapping("/updateKidswear")
	public String updateKidswear(@RequestBody Kidswear kidswear){
		Kidswear wear = kidswearDAO.registerKidswear(kidswear);
		if(wear != null){
			return "Kidswear record updated in the database";
		}
		return "Kidswear updation failed!!!";
	}
	
	@DeleteMapping("/deleteKidswear/{id}")
	public String deleteKidswear(@PathVariable("id") int id){
		kidswearDAO.deleteKidswear(id);
		return "Kidswear(" + id + ") record deleted Successfully!";
	}
	
	}

