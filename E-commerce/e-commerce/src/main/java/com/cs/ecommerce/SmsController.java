package com.cs.ecommerce;

import java.security.SecureRandom;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@RestController
public class SmsController {
	
	 private static final int OTP_LENGTH = 6;

	  public static String generateOTP() {
	    SecureRandom random = new SecureRandom();
	    StringBuilder otp = new StringBuilder();
	    for (int i = 0; i < OTP_LENGTH; i++) {
	      otp.append(random.nextInt(10));
	    }
	    return otp.toString();
	  }
	  
	  @GetMapping(value = "/sendSMS")
	     public ResponseEntity<String> sendSMS(@RequestParam("mobileNo") String mobileNo) {
		      System.out.print(mobileNo);
	         Twilio.init("AC849a760b926c31434c560fc32034741c", "70a2605881ebca6f76bf7b1829201297");
			 //Twilio.init(System.getenv("TWILIO_ACCOUNT_SID"), System.getenv("TWILIO_AUTH_TOKEN"));
	             final String OTP_TO_VERIFY = SmsController.generateOTP(); 
	             Message.creator(new PhoneNumber("<+91"+mobileNo+">"),
	                             new PhoneNumber("<+12058397502"), "Hello from ClickAndBuy"+" Your One Time Password(OTP) is "+ OTP_TO_VERIFY).create();

	             return new ResponseEntity<String>(OTP_TO_VERIFY, HttpStatus.OK);
	     }
	}


