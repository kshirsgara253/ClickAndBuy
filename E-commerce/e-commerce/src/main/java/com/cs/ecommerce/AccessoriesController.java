package com.cs.ecommerce;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.AccessoriesDAO;
import com.model.Accessories;

@RestController
public class AccessoriesController {
	
	@Autowired
	AccessoriesDAO accessoriesDAO;
	
	@GetMapping("/getAllAccessories")
	public List<Accessories> getAllAccessories(){
		return accessoriesDAO.getAllAccessoriess();
	}


	@GetMapping("/getAccessoriesById/{accessoryId}")
	public Accessories geAccessoriesById(@PathVariable("accessoryId") int accessoryId){
		Accessories acc = accessoriesDAO.getAccessoriesById(accessoryId);
		
		if(acc != null){
			return acc;
		}
		return new Accessories("Not found");
		
	}
	
	@GetMapping("/getAccessoriesByName/{accessoryName}")
	public Accessories getAccessoriesByName(@PathVariable("accessoryName") String accessoryName){
		Accessories acc = accessoriesDAO.getAccessoriesByName(accessoryName);
		
		if(acc != null){
			return acc;
			
		}
		return new Accessories("Not found");
	}
	
	@PostMapping("/registerAccessories")
	public String registerAccessories(@RequestBody Accessories accessory){
		Accessories acc = accessoriesDAO.registerAccessories(accessory);
		if(acc != null){
			return "Accessories registered into the database";
			
		}
		return "Accessories registration failed!!!";
	}
	
	@PutMapping("/updateAccessories")
	public String updateAccessories(@RequestBody Accessories accessory){
		Accessories acc = accessoriesDAO.registerAccessories(accessory);
		if(acc != null){
			return "Accessories record updated in the database";
		}
		return "Accessories updation failed!!!";
	}
	
	@DeleteMapping("/deleteAccessories/{accessoryId}")
	public String deleteAccessories(@PathVariable("accessoryId") int accessoryId){
		accessoriesDAO.deleteAccessories(accessoryId);
		return "Accessories(" + accessoryId + ") record deleted Successfully!";
	}

}


