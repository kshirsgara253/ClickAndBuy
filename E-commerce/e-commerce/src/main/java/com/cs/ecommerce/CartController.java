package com.cs.ecommerce;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CartDAO;
import com.dao.UserDAO;
import com.model.Cart;
import com.model.User;


@RestController
public class CartController {
	
	@Autowired
	CartDAO cartDAO;
	
	@Autowired
	UserDAO userDAO;
	
	@GetMapping("/getCartItems/{userId}")
	public List<Cart> getCartItems(@PathVariable("userId") int userId){
		List<Cart> cart = cartDAO.getCartItems(userId);
		if(cart != null){
			return cart;
		}
		return null;
	}
	
	@PostMapping("/registercart/{userId}")
	public String registerCart(@PathVariable("userId") int userId, @RequestBody Map<String, Object> data){
		User user = userDAO.getUserById(userId);
		 if (user == null) {
		        return "User with ID " + userId + " does not exist";
		    }
		 Cart cart = new Cart();
		    cart.setName(data.get("name").toString());
		    cart.setDescription(data.get("description").toString());
		    cart.setPrice(Double.parseDouble(data.get("price").toString()));
		    cart.setUser(user);
		
		Cart item = cartDAO.registerCart(cart);
		if(item != null){
			return "Product registered into the cart database";
			
		}
		return "Cart registration failed!!!";
	}
	
	
	
	@DeleteMapping("/deleteCart/{userId}")
	public String deleteCart(@PathVariable("userId") int userId){
		cartDAO.deleteByUserId(userId);
		return "Laptop(" + userId + ") record deleted Successfully!";
	}
	
}

