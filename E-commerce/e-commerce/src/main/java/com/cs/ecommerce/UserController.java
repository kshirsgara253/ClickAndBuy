package com.cs.ecommerce;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.UserDAO;
import com.model.User;

@RestController
public class UserController {
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	UserDAO userDAO;
	
	
	
	@GetMapping("/getAllUser")
		public List<User> getAllUser(){
			return userDAO.getAllUsers();
		}
	
	
	@GetMapping("/getUserById/{userId}")
	public User geUserById(@PathVariable("userId") int userId){
		User use = userDAO.getUserById(userId);
		
		if(use != null){
			return use;
		}
		return new User("Not found");
		
	}
	
	@GetMapping("/getUser/{emailId}/{password}")
	public User getUser(@PathVariable("emailId") String emailId, @PathVariable("password") String password) {
		
		User user = userDAO.getUserByEmail(emailId);
		if(user == null){
			return null;
		}
		String encryptedPassword = user.getPassword();
		if (!passwordEncoder.matches(password, encryptedPassword)) {
		     return null;
		}
		return user;
	}
	
	
	
	@GetMapping("/getUserByName/{userName}")
	public User getUserByName(@PathVariable("userName") String userName){
		User use = userDAO.getUserByName(userName);
		
		if(use != null){
			return use;
			
		}
		return new User("Not found");
	}
	
	@PostMapping("/registerUser")
	public String registerUser(@RequestBody User user){
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		User use = userDAO.registerUser(user);		
		if(use != null){
			return "User registered into the database";
			
		}
		return "User registration failed!!!";
	}
	
	@PutMapping("/updateUser")
	public String updateUser(@RequestBody User user){
		User use = userDAO.registerUser(user);
		if(use != null){
			return "User record updated in the database";
		}
		return "User updation failed!!!";
	}
	
	@DeleteMapping("/deleteUser/{userId}")
	public String deleteUser(@PathVariable("userId") int userId){
		userDAO.deleteUser(userId);
		return "User(" + userId + ") record deleted Successfully!";
	}
	
}
