package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Mobile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String brand;

    @ManyToOne
    @JoinColumn(name = "electronics_id")
    private Electronics electronics;

	public Mobile() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public Mobile(Long id, String brand) {
		super();
		this.id = id;
		this.brand = brand;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Electronics getElectronics() {
		return electronics;
	}

	public void setElectronics(Electronics electronics) {
		this.electronics = electronics;
	}

	@Override
	public String toString() {
		return "Mobile [id=" + id + ", brand=" + brand + ", electronics=" + electronics + "]";
	}

    
}

