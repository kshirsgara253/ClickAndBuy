package com.model;

public class SmartWearable {
	private Long id;
	private String brandName;
	
	@ManyToOne
    @JoinColumn(name = "electronics_id")
    private Electronics electronics;

	public SmartWearable() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SmartWearable(Long id, String brandName) {
		super();
		this.id = id;
		this.brandName = brandName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public Electronics getElectronics() {
		return electronics;
	}

	public void setElectronics(Electronics electronics) {
		this.electronics = electronics;
	}

	@Override
	public String toString() {
		return "SmartWearable [id=" + id + ", brandName=" + brandName + ", electronics=" + electronics + "]";
	}
	
	
	
	
}
