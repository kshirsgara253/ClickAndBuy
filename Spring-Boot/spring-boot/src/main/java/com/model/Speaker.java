package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Speaker {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String brandName;
	
	@ManyToOne
    @JoinColumn(name = "electronics_id")
    private Electronics electronics;

	public Speaker() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Speaker(Long id, String brandName) {
		super();
		this.id = id;
		this.brandName = brandName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public Electronics getElectronics() {
		return electronics;
	}

	public void setElectronics(Electronics electronics) {
		this.electronics = electronics;
	}

	@Override
	public String toString() {
		return "Speaker [id=" + id + ", brandName=" + brandName + ", electronics=" + electronics + "]";
	}
	
	

}
